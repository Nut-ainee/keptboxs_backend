// optional: allow environment to specify port
const port = process.env.PORT || 8008;
// const s_port = process.env.PORT || 8082;
const path = __dirname;

var axios = require('axios');
var FormData = require('form-data');
var fs = require('fs');
var http = require('http')
var mysql = require('mysql');
var unirest = require('unirest');

var https = require('https');
// var privateKey  = fs.readFileSync('/etc/letsencrypt/live/chp.giantiot.com/privkey.pem', 'utf8');
// var certificate = fs.readFileSync('/etc/letsencrypt/live/chp.giantiot.com/cert.pem', 'utf8');
// var credentials = {key: privateKey, cert: certificate};

// wire up the module
const express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');


var mqtt = require('mqtt');
const { json } = require('body-parser');
const axiosConfig = {
  headers: {
    'Authorization': 'Bearer A322f56725006518fbf26ff917a151289674024b3604642168c34f6096ceb09458808ea191e3e4a518a800c6e733b3ab3'
  }
};

// create server instance
const app = express();
app.use(express.json());
app.use(bodyParser.json());
app.use(express.static(__dirname + '/dist'));

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', "*");   // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');    // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Credentials', true);// Set to true if you need the website to include cookies in the requests sent 
  next();  // Pass to next layer of middleware
});

//*** */

const __api_v = "/api/v1/";
//http://203.150.184.156:8081/
const BACKEND_URL = "http://192.168.26.1:8081";
// const BACKEND_URL = "http://localhost:8008";
// const FRONTEND_URL = "http://gravity.giantiot.com:8002"
const FRONTEND_URL = "http://203.150.184.156:8002";
const TOPIC_UNLOCK = "@mgs/subdata/lock"; // topic : req unlock
const TOPIC_SUB_UNLOCK = "@mgs/pubdata/lock";
const TOPIC_SETLOCK = "@mgs/subdata/btn/lock";// topic : req set MAC lock, gateway
const TOPIC_SUB_SETLOCK = "@mgs/pubdata/setlock";
const BOT_ID = "B413b411cf8be57bb938b44012e1dc428";
// MQTT config
const MQTT_SERVER = "18.140.173.239";
const MQTT_PORT = "1883";
const MQTT_USER = "chp-lab";
const MQTT_PASSWORD = "atop3352";
// db config

/* docker run -d --name mysql_database -e MYSQL_USER=user -e MYSQL_PASSWORD=pass -e MYSQL_DATABASE=db -p 3306:3306 rhscl/mysql-56-rhel7 */

// var db_config = {
//   host: "gravity.giantiot.com",
//   user: "admin",
//   password: "QWer!@34",
//   database: "db_keptbox",
//   port: 3306
// };
var db_config = {
  host: "192.168.26.1",
  // host: "203.150.184.156",
  user: "root",
  password: "QWer!@34",
  database: "db_keptbox",
  port: 3306
};
var con;

function handleDisconnect() {
  con = mysql.createConnection(db_config); // Recreate the connection, since
  // the old one cannot be reused.

  con.connect(function (err) {              // The server is either down
    if (err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } else {
      console.log("mysql Connected!");
    }                                   // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  con.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();

/*----------------------------------------------------------------------------*/


// Connect MQTT
var client = mqtt.connect({
  host: MQTT_SERVER,
  port: MQTT_PORT,
  username: MQTT_USER,
  password: MQTT_PASSWORD
});

client.on('connect', function () {
  // Subscribe any topic
  console.log("MQTT Connect");
  client.subscribe(TOPIC_SUB_SETLOCK, function (err) {
    if (err) { console.log(err); }
  });
  client.subscribe(TOPIC_SUB_UNLOCK, function (err) {
    if (err) { console.log(err); }
  });
  client.subscribe('test', function (err) {
    if (err) { console.log(err); }
  });
});

// Receive Message and print on terminal
client.on('message', function (topic, message) {
  var response = message.toString();
  if (topic == TOPIC_SUB_SETLOCK) {
    console.log("--->TOPIC_SUB_SETLOCK");
    // console.log({ "res_setMAC": message.toString() });
    var string = response;
    var data = JSON.parse(string);
    console.log(data);
    setMAC_Status(data);
  } else if (topic == TOPIC_SUB_UNLOCK) {
    console.log("--->TOPIC_SUB_UNLOCK");
    console.log(response);
    var data = JSON.parse(response);
    // console.log(data);
    var box_sn = "lys" + data.sn;
    var _box_sn = box_sn.replace(/:/g, "");
    var sql_update_status_lock = "UPDATE mac_devices SET status_lock ='" + data.status + "' WHERE mac_devices.box_sn = '" + _box_sn + "'";
    con.query(sql_update_status_lock, function (err, data, fields) {
      if (err) {
        console.log("[error]", err);
      } else {
        console.log({ "message": "Update Success" });
      }
    });
    if (data.status == "gateway unlocking") {
      axios.post(BACKEND_URL + __api_v + 'keptboxs/logs?at=gateway&sn=' + _box_sn.toLowerCase())  // logs unlock
        .then(function (response) {
          // console.log({ "message": response.data });
          setTimeout(() => {
            console.log("Delayed for 10 second >> check status in mac table");
            var sql_check = "SELECT * FROM mac_devices  WHERE box_sn = '" + _box_sn.toLowerCase() + "'";
            con.query(sql_check, function (err, res, fields) {
              if (err) {
                console.log("[error]", err);
              } else {
                if (res.length !== 0) {
                  var string = JSON.stringify(res);
                  var _data = JSON.parse(string);
                  var getData = _data[0];
                  console.log({ "check status": res.length });
                  if (getData.status_lock == "gateway unlocking") {
                    con.query("DELETE FROM opening_logs WHERE box_sn = '" + _box_sn.toLowerCase() + "'",
                      function (err, data, fields) {
                        if (err) {
                          console.log("[error]", err);
                        } else {
                          console.log("unlock by gate way fail");
                        }
                      });
                  }
                } else {
                  console.log("[ no 'box_sn' in table 'mac_devices' ]");
                }
              }
            });
          }, "20000")
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    // }
  } else {
    console.log({ [topic]: response });
  }
});

function times() {
  let date_ob = new Date();
  let date = ("0" + date_ob.getDate()).slice(-2);
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  let year = date_ob.getFullYear();
  let hours = date_ob.getHours();
  let minutes = date_ob.getMinutes();
  let seconds = date_ob.getSeconds();
  let d = year + "-" + month + "-" + date + "_" + hours + "." + minutes + "." + seconds;
  return d;
}

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'dist/upload');
  },
  filename: function (req, file, cb) {
    cb(null, times() + '-' + file.originalname);
  }
});

var upload = multer({
  storage: storage
});

setTimeout(() => {
  console.log(toThaiDateString(new Date(), "req_create"));
}, 2000);

app.post('/api/v1/test', (req, res) => {
  var req = unirest('POST', BACKEND_URL + '/helloo')
    .end(function (res) {
      if (res.error) {
        console.log(res.error);
        // throw new Error(res.error);
      } else {
        console.log(res);
      }
    });
})

app.post("/helloo", (req, res) => {
  res.json({ message: "Hello World." });
  console.log(res);
});


app.post('/api/image-upload', upload.single('fileToUpload'), (req, res) => {
  console.log("--->/api/image-upload");
  console.log(req.query.macCam);
  var path = req.file.path;
  var macCam = req.query.macCam;
  var sql_select = "SELECT box_sn, box_name, created_by, user_id, user_permis  FROM devices WHERE box_sn = (SELECT box_sn FROM mac_devices WHERE mac_cam = '" + macCam + "')";
  con.query(sql_select, function (err, data, fields) {
    if (err) {
      console.log("[error]", err);
      res.send("fail");
    } else {
      var string = JSON.stringify(data);
      var _data = JSON.parse(string);
      // console.log(_data);
      // console.log(_data.length);
      var res_send = true;
      for (let i = 0; i < _data.length; i++) {
        const element = _data[i];
        let message_re = "กล่อง" + element.box_name + " : มีพัสดุมาส่ง  📮 \r\n" + toThaiDateString(new Date(), "image-upload");
        let req_body = {
          "to": element.user_id,
          "bot_id": BOT_ID,
          "type": "text",
          "message": message_re,
          "custom_notification": "เปิดอ่านข้อความใหม่จากทางเรา"
        };
        axios.post('https://chat-api.one.th/message/api/v1/push_message', req_body, axiosConfig)
          .then(function (response) {
            // console.log(response);
            console.log({ "msg": "to " + element.user_id });
          })
          .catch(function (error) {
            console.log({ "error": error.response.data });
          });

        var req = unirest('POST', 'https://chat-api.one.th/message/api/v1/push_message')
          .headers({
            'Authorization': 'Bearer A322f56725006518fbf26ff917a151289674024b3604642168c34f6096ceb09458808ea191e3e4a518a800c6e733b3ab3'
          })
          .field('to', element.user_id)
          .field('bot_id', BOT_ID)
          .field('type', 'file')
          .attach('file', path)
          .end(function (response) {
            if (response.error) {
              console.log(response.error);
              res_send = false;
            } else {
              // console.log(response.body);
              console.log({ "img": "to " + element.user_id });
              // console.log(response.body.status);
              var image_url = response.body.message.text;
              if (element.user_permis == 'admin') {
                var sql_insert_logs = "INSERT INTO senders_logs (box_sn, image)VALUES ( '" + element.box_sn + "', '" + image_url + "') ";
                con.query(sql_insert_logs, function (err, data, fields) {
                  if (err) {
                    console.log("[error]", err);
                    res_send = false;
                  } else {
                    console.log("insert logs success");
                    res_send = true;
                  }
                })
              }
              res_send = true;
            }
          });
      }
      if (res_send) {
        res.send("ok");
      } else {
        res.send("fail");
      }
    }
  });
});



app.get(__api_v + 'getvoltage', (req, res) => {
  console.log("macAddress " + req.query.macCam);
  console.log("voltage " + req.query.volt);
  var macCam = req.query.macCam;
  var volt = req.query.volt;
  var level = 0;
  if (volt >= 3.4) {
    level = 100;
  } else if (volt >= 3.35) {
    var cal = 3.4 - 3.35;
    level = 90 + (cal - (3.4 - volt)) / (cal / 10) | 0;
    console.log(level);
  } else if (volt >= 3.32) {
    var cal = 3.35 - 3.32;
    level = 80 + (cal - (3.35 - volt)) / (cal / 10) | 0;
  } else if (volt >= 3.30) {
    var cal = 3.32 - 3.30;
    level = 70 + (cal - (3.32 - volt)) / (cal / 10) | 0;
  } else if (volt >= 3.27) {
    var cal = 3.30 - 3.27;
    level = 60 + (cal - (3.30 - volt)) / (cal / 10) | 0;
  } else if (volt >= 3.26) {
    var cal = 3.27 - 3.26;
    level = 50 + (cal - (3.27 - volt)) / (cal / 10) | 0;
  } else if (volt >= 3.25) {
    var cal = 3.26 - 3.25;
    level = 40 + (cal - (3.26 - volt)) / (cal / 10) | 0;
  } else if (volt >= 3.22) {
    var cal = 3.25 - 3.22;
    level = 30 + (cal - (3.25 - volt)) / (cal / 10) | 0;
  } else if (volt >= 3.20) {
    var cal = 3.22 - 3.20;
    level = 20 + (cal - (3.22 - volt)) / (cal / 10) | 0;
  } else if (volt >= 3.00) {
    var cal = 3.20 - 3.00;
    level = 10 + (cal - (3.20 - volt)) / (cal / 10) | 0;
  } else if (volt < 3.00) {
    var cal = 3.00 - 2.50;
    level = (cal - (3.00 - volt)) / (cal / 10) | 0;
  }
  console.log("battery level : " + level);
  var sql_update = "UPDATE devices SET batt_level ='" + level + "' WHERE box_sn = (SELECT box_sn FROM mac_devices WHERE mac_cam = '" + macCam + "' )";
  // console.log(sql_update);
  con.query(sql_update, function (err, data, fields) {
    if (err) {
      console.log("[error]", err);
      return res.status(400).json({ error: { "code": 400, "message": "Update fail" } });
    } else {
      console.log({ "message": "Update Success" });
      console.log(data);
      return res.status(200).json({ data: { "message": "Update Success" } });
    }
  });
})


// http://localhost:8080/keptlock/unlock?sn=LYSFC58FA3BD726
app.post(__api_v + "keptlock/unlock", (req, res) => {
  console.log("--->/api/v1/keptbox/unlock");
  const _box_sn = req.query.sn;
  con.query("SELECT * FROM mac_devices WHERE box_sn = '" + _box_sn.toLowerCase() + "'", function (err, data, fields) {
    if (err) {
      console.log("[error]", err);
    } else {
      console.log(data);
      var string = JSON.stringify(data);
      var _data = JSON.parse(string);
      var getData = _data[0];
      console.log({ "getData": getData });
      var req_body = {
        "sn": getData.mac_lock,
        "gateway": getData.mac_gateway
      }
      var payload = JSON.stringify(req_body);
      // console.log({ "req unlock": payload });
      client.publish(TOPIC_UNLOCK, payload);
      res.send("ok");
    }
  });
});

// http://localhost:8080/api/v1/keptlock/setunlock?gateway=(EC626093A128)&sn=(LYSFC58FA3BD726)     ????&cam=(0CB815F2F28C)
app.post(__api_v + "keptlock/setunlock", (req, res) => {
  console.log("--->/api/v1/keptbox/setunlock");
  const _gateway = req.query.gateway;
  const _box_sn = req.query.sn;
  // const _cam = req.query.cam;
  var _lock = _box_sn.substring(3);
  var req_body = {
    "sn": str_MAC(_lock),
    "gateway": str_MAC(_gateway),
  }
  var payload = JSON.stringify(req_body);
  console.log({ "req": payload });
  client.publish(TOPIC_SETLOCK, payload);
  res.send("MAC setting");
});

app.get(__api_v + "receivebox_his", (req, res) => {
  console.log("--->/api/v1/receivebox_his");
  const _box_sn = req.query.sn;
  con.query("SELECT id,open_at, image FROM senders_logs WHERE box_sn ='" + _box_sn + "' ORDER BY open_at DESC LIMIT 10", function (err, data, fields) {
    if (err) {
      console.log("[error]", err);
      return res.status(400).json({ error: { "code": 400, "message": err.code } });
    } else {
      var string = JSON.stringify(data);
      var _data = JSON.parse(string);
      console.log('>> DATA: ', _data);
      return res.status(200).json({ data: _data });
    }
  });
});

app.get(__api_v + "users", (req, res) => {
  con.query("SELECT * FROM users", function (err, data, fields) {
    if (err) {
      console.log("[error]", err);
      return res.status(400).json({ error: { "code": 400, "message": err.code } });
    } else {
      console.log(data);
      res.json({ data: data });
    }
  });
});

app.get(__api_v + "test", (req, res) => {
  axios.get(BACKEND_URL + __api_v + "keptboxs?id=nur-ainee.toh@one.th")
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
});

// setTimeout(() => {
//   axios.get(BACKEND_URL + __api_v + "keptboxs?id=nur-ainee.toh@one.th")
//     .then(response => {
//       console.log(response.data);
//     })
//     .catch(error => {
//       console.log(error);
//     });
// }, 2000);


/*----------------------------------------------------------------------------*/
app.get(__api_v + "keptboxs", (req, res) => {
  const one_email = req.query.id;
  console.log("--->/api/v1/keptboxs");
  con.query("SELECT box_sn, box_name, user_permis, batt_level, location FROM devices WHERE created_by = '" + one_email + "'",
    function (err, data, fields) {
      if (err) {
        console.log("[error]", err);
        return res.status(400).json({ error: { "code": 400, "message": err.code } });
      } else {
        // console.log(data);
        var string = JSON.stringify(data);
        var _data = JSON.parse(string);
        console.log('>> DATA: ', _data);
        return res.status(200).json({ data: _data });
      }
    });
});

//------ API EDIT -------//
app.get(__api_v + "edit", function (req, res) {
  // const user_id = req.query.id; //one id
  const sn = req.query.sn; // sn box
  console.log("--->/api/v1/edit");
  // console.log('>> user_id: ', user_id);
  console.log('>> box_sn: ', sn);
  var sql = "SELECT * FROM keptboxs  WHERE box_sn = '" + sn + "'";
  con.query(sql, function (err, data, fields) {
    if (err) {
      console.log("[error]", err);
      return res.status(400).json({ error: { "code": 400, "message": err.code } });
    } else if (data.length === 0) {
      console.log("[error]", "box sn is empty");
      return res.status(400).json({ error: { "code": 400, "message": "no permission" } });
    } else {
      var string = JSON.stringify(data);
      var _data = JSON.parse(string);
      // console.log('>> DATA: ', _data);
      var res_data = _data[0];
      var val_phone = [];
      for (let index = 0; index < _data.length; index++) {
        val_phone.push(_data[index].phone_num);
      }
      // console.log(val_phone);
      res_data.phone_num = val_phone;
      console.log(res_data);
      return res.status(200).json({ data: res_data });
    }
  });
});

//------ API UPDATE -------//
app.put(__api_v + "update", (req, res) => {
  const user_id = req.query.id; //one id
  const sn = req.query.sn;
  console.log("--->/api/v1/update");
  console.log('>> box_sn: ', sn);
  console.log("[body]", req.body);
  var data = req.body;
  // var _sn = data.box_sn;
  var _name = data.box_name;
  var _location = data.location;
  var _phone = data.phone_num;
  var _email = data.email;
  var sql_check = "SELECT * FROM keptboxs  WHERE box_sn = '" + sn + "'";
  con.query(sql_check, function (err, data, fields) {
    if (err) {
      console.log("[error]", err);
      return res.status(400).json({ error: { "code": 400, "message": "Update fail" } });
    } else {
      var string = JSON.stringify(data);
      var _data = JSON.parse(string);
      // console.log('>> DATA: ', _data);
      const newlen_phone = _phone.length;
      const oldlen_phone = _data.length;
      if (newlen_phone > oldlen_phone) {
        console.log(" newlen_phone > oldlen_phone");
        // const num = newlen_phone - oldlen_phone;
        for (let upd = 0; upd < oldlen_phone; upd++) {
          // console.log({ "box id": _data[upd].box_id });
          var sql_update = "UPDATE keptboxs SET box_sn = '" + sn + "', box_name ='" + _name + "', location = '" + _location + "', phone_num = '" + _phone[upd] + "', update_by = '" + _email + "' WHERE keptboxs.box_id = '" + _data[upd].box_id + "'";
          // console.log(sql_update);
          con.query(sql_update, function (err, data, fields) {
            if (err) {
              console.log("[error]", err);
              return res.status(400).json({ error: { "code": 400, "message": "Update fail" } });
            }
          });
          if (upd == oldlen_phone - 1) {
            var value_ins = [];
            var sql_ins = "INSERT INTO keptboxs (box_sn, box_name, location, phone_num, created_by) VALUES ?";
            for (let ins = upd + 1; ins < newlen_phone; ins++) {
              console.log({ [ins]: _phone[ins] });
              value_ins.push([sn, _name, _location, _phone[ins], _email]);
            }
            con.query(sql_ins, [value_ins], function (err, data, fields) {
              if (err) {
                console.log("[error]", err);
                return res.status(400).json({ error: { "code": 400, "message": "Update fail" } });
              } else {
                var sql_update = "UPDATE devices SET box_name ='" + _name + "', location = '" + _location + "', update_by = '" + _email + "' WHERE devices.box_sn = '" + sn + "'";
                console.log(sql_update);
                con.query(sql_update, function (err, data, fields) {
                  if (err) {
                    console.log("[error]", err);
                    return res.status(400).json({ error: { "code": 400, "message": "Update fail" } });
                  } else {
                    console.log({ "message": "Update Success" });
                    return res.status(200).json({ data: { "message": "Update Success" } });
                  }
                });
              }
            });
          }
        }
      } else if (newlen_phone < oldlen_phone) {
        console.log(" newlen_phone < oldlen_phone");
        // const num = oldlen_phone - newlen_phone; 
        for (let upd = 0; upd < newlen_phone; upd++) {
          // console.log({ "box id": _data[upd].box_id });
          var sql_update = "UPDATE keptboxs SET box_sn = '" + sn + "', box_name ='" + _name + "', location = '" + _location + "', phone_num = '" + _phone[upd] + "', update_by = '" + _email + "' WHERE keptboxs.box_id = '" + _data[upd].box_id + "'";
          // console.log(sql_update);
          con.query(sql_update, function (err, data, fields) {
            if (err) {
              console.log("[error]", err);
              return res.status(400).json({ error: { "code": 400, "message": "Update fail" } });
            }
          });
          if (upd == newlen_phone - 1) {
            for (let del = upd + 1; del < oldlen_phone; del++) {
              // console.log({ [del]: _data[del].phone_num });
              var sql_del = "DELETE FROM keptboxs WHERE box_id = " + _data[del].box_id;
              // console.log({ [del]: sql_del });
              con.query(sql_del, function (err, data, fields) {
                if (err) {
                  console.log("[error]", err);
                  return res.status(400).json({ error: { "code": 400, "message": "Update fail" } });
                } else if (del == oldlen_phone - 1) {
                  var sql_update = "UPDATE devices SET box_name ='" + _name + "', location = '" + _location + "', update_by = '" + _email + "' WHERE devices.box_sn = '" + sn + "'";
                  console.log(sql_update);
                  con.query(sql_update, function (err, data, fields) {
                    if (err) {
                      console.log("[error]", err);
                      return res.status(400).json({ error: { "code": 400, "message": "Update fail" } });
                    } else {
                      console.log({ "message": "Update Success" });
                      return res.status(200).json({ data: { "message": "Update Success" } });
                    }
                  });
                }
              });
            }
          }
        }
      } else {
        for (let upd = 0; upd < newlen_phone; upd++) {
          // console.log({ "box id": _data[upd].box_id });
          var sql_update = "UPDATE keptboxs SET box_sn = '" + sn + "', box_name ='" + _name + "', location = '" + _location + "', phone_num = '" + _phone[upd] + "', update_by = '" + _email + "' WHERE keptboxs.box_id = '" + _data[upd].box_id + "'";
          // console.log(sql_update);
          con.query(sql_update, function (err, data, fields) {
            if (err) {
              console.log("[error]", err);
              return res.status(400).json({ error: { "code": 400, "message": "Update fail" } });
            } else if (upd == newlen_phone - 1) {
              var sql_update = "UPDATE devices SET box_name ='" + _name + "', location = '" + _location + "', update_by = '" + _email + "' WHERE devices.box_sn = '" + sn + "'";
              console.log(sql_update);
              con.query(sql_update, function (err, data, fields) {
                if (err) {
                  console.log("[error]", err);
                  return res.status(400).json({ error: { "code": 400, "message": "Update fail" } });
                } else {
                  console.log({ "message": "Update Success" });
                  return res.status(200).json({ data: { "message": "Update Success" } });
                }
              });
            }
          });
        }
      }
    }
  });
});

//------ API subscriber -------//
app.get(__api_v + "subscriber", function (req, res) {
  // const user_id = req.query.id; //one id
  const sn = req.query.sn; // sn box
  console.log("--->/api/v1/subscriber");
  // console.log('>> user_id: ', user_id);
  console.log('>> box_sn: ', sn);
  var sql = "SELECT * FROM devices  WHERE box_sn = '" + sn + "'";
  con.query(sql, function (err, data, fields) {
    if (err) {
      console.log("[error]", err);
      return res.status(400).json({ error: { "code": 400, "message": err.code } });
    } else if (data.length === 0) {
      console.log("[error]", "box sn is empty");
      return res.status(400).json({ error: { "code": 400, "message": "no permission" } });
    } else {
      var string = JSON.stringify(data);
      var _data = JSON.parse(string);
      console.log('>> DATA: ', _data);
      return res.status(200).json({ data: _data });
    }
  });
});

function toThaiDateString(date, api) {
  console.log("date :" + date);
  let monthNames = [
    "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
    "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม.",
    "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"
  ];
  let dayNames = ["วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์"]

  let year = date.getFullYear() + 543;
  let month = monthNames[date.getMonth()];
  let numOfDay = date.getDate();
  let day = dayNames[date.getDay()];
  let hour = date.getHours().toString().padStart(2, "0");
  let minutes = date.getMinutes().toString().padStart(2, "0");
  let second = date.getSeconds().toString().padStart(2, "0");
  if (api == "image-upload") {
    return day + "ที่ " + numOfDay + " " + month + " " + year + "\r\nเวลา " + hour + ":" + minutes + ":" + second + " น. ";
  } else if (api == "req_create") {
    return day + "ที่ " + numOfDay + " " + month + " " + year + " " + hour + ":" + minutes + " น. ";
  }
}

app.post(__api_v + "keptboxs/req_create", (req, res) => {
  console.log("--->/api/v1/keptboxs/req_create");
  console.log("[body]", req.body);
  var data = req.body;
  if ('onechat_token' in data && 'qr_result' in data) {
    if (data.onechat_token == "null" || data.qr_result == "null") {
      console.log({ "error": "key has null" });
      return res.status(400).json({ error: { "code": 400, "message": "request fail" } });
    } else {
      console.log({ "data": "ok" });
      var box_sn = data.qr_result.substring(0, 15).toLowerCase();
      var user_token = data.onechat_token;
      var req_getProfile = {
        "bot_id": BOT_ID,
        "source": user_token
      }
      // console.log(req_getProfile);
      axios.post('https://chat-api.one.th/manage/api/v1/getprofile', req_getProfile, axiosConfig)
        .then(function (response) {
          // console.log(response.data);
          if ("data" in response.data) {
            // console.log("have data");
            var user_id_user = response.data.data.user_id;
            var email_user = response.data.data.email;
            var nickname = response.data.data.nickname
            // console.log(user_id_user);
            var sql_getBOX = "SELECT * FROM devices WHERE box_sn ='" + box_sn + "'";
            // var sql_getBOX = "SELECT * FROM devices WHERE box_sn = '" + box_sn + "' AND user_permis = 'admin'";
            con.query(sql_getBOX, function (err, data, fields) {
              if (err) {
                console.log("[error]", err);
                return res.status(400).json({ error: { "code": 400, "message": "request fail" } });
              } else {
                if (data.length == 0) {
                  console.log({ "message": "no keptbox" });
                  return res.status(400).json({ error: { "code": 400, "message": "request fail" } });
                } else {
                  var string = JSON.stringify(data);
                  var _data = JSON.parse(string);
                  // console.log({ "data": _data });
                  var getData = _data[0];
                  var user_id_admin = getData.user_id;
                  let check_req = true;
                  // console.log({ "data": getData });
                  for (let i = 0; i < _data.length; i++) {
                    console.log({ [i]: _data[i].user_id });
                    if (_data[i].user_id == user_id_user) {
                      console.log("คุณมีกล่องอยู่แล้ว");
                      check_req = false;
                      break;
                    }
                  }
                  if (check_req) {
                    // console.log("ขอ permis ได้");
                    var req_approve = {
                      "to": user_id_admin,
                      "bot_id": BOT_ID,
                      "type": "approve_message",
                      "custom_notification": "เปิดอ่านข้อความใหม่จากทางเรา",
                      "title": "คำขอใช้งานกล่อง : " + getData.box_name,
                      "short_detail": toThaiDateString(new Date(), "req_create"),
                      "detail": "คุณ " + nickname + " ได้ส่งคำขอใช้งานกล่อง [ " + getData.box_name + " ] ของคุณ " + toThaiDateString(new Date(), "req_create"),
                      "image": BACKEND_URL + "/Box-03.png",
                      "choice": [
                        {
                          "label": "อนุญาต",
                          "type": "approve",
                          "payload": box_sn + "_" + user_id_user + "_" + email_user
                        },
                        {
                          "label": "ไม่อนุญาต",
                          "type": "cancel",
                          "payload": box_sn + "_" + user_id_user + "_" + email_user
                        }
                      ]
                    }
                    axios.post('https://chat-api.one.th/message/api/v1/push_message', req_approve, axiosConfig)
                      .then(function (response) {
                        // console.log(response.data);
                      })
                      .catch(function (error) {
                        console.log({ "error": error.response.config.data });
                      });
                    return res.status(200).json({ data: { "message": "request Success" } });
                  } else {
                    return res.status(400).json({ error: { "code": 400, "message": "request duplicate" } });
                  }
                }
              }
            });
          } else {
            console.log({ "error": error });
            return res.status(400).json({ error: { "code": 400, "message": "request fail" } });
          }
        })
        .catch(function (error) {
          console.log({ "error": error.response.data });
          return res.status(400).json({ error: { "code": 400, "message": "request fail" } });
        });
    }
  } else {
    console.log({ "error": "key has undefined" });
    return res.status(400).json({ error: { "code": 400, "message": "request fail" } });
  }
})

//----- api create   //
app.post(__api_v + "keptboxs/create", (req, res) => {
  console.log("--->/api/v1/keptboxs/create");
  console.log("[body]", req.body);
  var data = req.body;
  if ("box_sn" in data && "mac_gateway" in data && "mac_cam" in data && "box_name" in data && "location" in data && "phone_num" in data && "email" in data && "user_id" in data) {
    console.log("have keys");
    var _sn = data.box_sn;
    var _gateway = data.mac_gateway;
    var _cam = data.mac_cam;
    var _name = data.box_name;
    var _location = data.location;
    var _phone = data.phone_num;
    var _email = data.email;
    var _user_id = data.user_id   //'U15cb6c487f4956abac6187dd34eaea07'
    if (_sn == "null" || _gateway == "null" || _cam == "null" || _user_id == "null") {
      console.log({ "response": "some param is null" });
      return res.status(400).json({ error: { "code": 400, "message": "Register fail" } });
    } else {
      con.query("SELECT * FROM devices WHERE box_sn = '" + _sn.toLowerCase() + "'",
        function (err, data, fields) {
          if (err) {
            console.log("[error]", err);
            return res.status(400).json({ error: { "code": 400, "message": "Register fail" } });
          } else {
            if (data.length !== 0) {
              console.log({ "message": "Duplicate box" });
              return res.status(400).json({ error: { "code": 400, "message": "Duplicate box" } }); //มีกล่องแล้ว
            } else {
              var _box_sn = _sn.toLowerCase();
              var box_sn = _box_sn.substring(3);
              var _mac_lock = str_MAC(box_sn);// set MAC format
              var _mac_gateway = str_MAC(_gateway);
              var _mac_cam = str_MAC(_cam);
              // call API SET UNLOCK here ;
              // var call_setunlock = "http://gravity.giantiot.com:8081/api/v1/keptlock/setunlock?gateway=" + _mac_gateway + "&sn=" + _sn;
              var call_setunlock = BACKEND_URL + __api_v + "keptlock/setunlock?gateway=" + _gateway + "&sn=" + _box_sn;
              axios.post(call_setunlock)
                .then(function (response) {
                  console.log({ "status setlock": response.data });
                  var sql = "INSERT INTO keptboxs (box_sn, box_name, location, phone_num, created_by) VALUES ?";
                  var values = [];
                  for (let index = 0; index < _phone.length; index++) {
                    values.push([_box_sn, _name, _location, _phone[index], _email]);
                  }
                  //insert keptboxs TABLE
                  con.query(sql, [values], function (err, data, fields) {
                    if (err) {
                      console.log("[error]", err);
                      return res.status(400).json({ error: { "code": 400, "message": "Register fail" } });
                    } else {
                      // insert devices TABLE
                      var decives = "INSERT INTO devices (box_sn, box_name, location, created_by, user_id) VALUES ?";
                      var val_device = [[_box_sn, _name, _location, _email, _user_id]];
                      con.query(decives, [val_device], function (err, data, fields) {
                        if (err) {
                          console.log("[error]", err);
                          return res.status(400).json({ error: { "code": 400, "message": "Register fail" } });
                        } else {
                          var sql_mac_devices = "INSERT INTO mac_devices (box_sn, mac_gateway, mac_lock, mac_cam) VALUES ('" + _box_sn + "','" + _mac_gateway + "','" + _mac_lock + "','" + _mac_cam + "')";
                          con.query(sql_mac_devices, function (err, data, fields) {
                            if (err) {
                              console.log("[error]", err);
                              return res.status(400).json({ error: { "code": 400, "message": "Register fail" } });
                            } else {
                              //check status set MAC
                              setTimeout(() => {
                                console.log("Delayed for 10 second.");
                                //SELECT mac_devices.box_sn ,mac_devices.status FROM `mac_devices` WHERE box_sn = "lysfc58fa3bd726"
                                var sql_check = "SELECT box_sn, status  FROM mac_devices  WHERE box_sn = '" + _box_sn + "'"; //[box_sn] = lysfc58fa3bd726
                                // console.log({ "sql_check": sql_check });
                                con.query(sql_check, function (err, data, fields) {
                                  if (err) {
                                    console.log("[error]", err);
                                    return res.status(400).json({ error: { "code": 400, "message": "Duplicate box" } });
                                  } else {
                                    // console.log({ "check status": data });
                                    console.log({ "check status": data.length });
                                    if (data.length !== 0) {
                                      var string = JSON.stringify(data);
                                      var _data = JSON.parse(string);
                                      var getData = _data[0];
                                      console.log("[ status ] " + getData.status);
                                      if (getData.status == "match") {
                                        console.log({ "message": "Register Success" });
                                        return res.status(200).json({ data: { "message": "Register Success" } });
                                      } else {
                                        axios.post(BACKEND_URL + __api_v + "keptboxs/delete?sn=" + _box_sn)
                                          .then(function (response) {
                                            // console.log("[error]", response.data);
                                            return res.status(400).json({ error: { "code": 400, "message": "Duplicate box" } });
                                          })
                                          .catch(function (error) {
                                            console.log("[error]", error);
                                            return res.status(400).json({ error: { "code": 400, "message": "Duplicate box" } });
                                          });
                                      }
                                    } else {
                                      // http://localhost:8080/api/v1/keptboxs/delete?sn=LYSFC58FA3BD726
                                      axios.post(BACKEND_URL + __api_v + "keptboxs/delete?sn=" + _box_sn)
                                        .then(function (response) {
                                          // console.log("[error]", response.data);
                                          return res.status(400).json({ error: { "code": 400, "message": "Duplicate box" } });
                                        })
                                        .catch(function (error) {
                                          console.log("[error]", error);
                                          return res.status(400).json({ error: { "code": 400, "message": "Duplicate box" } });
                                        });
                                    }
                                  }
                                });
                              }, "10000")
                            }
                          });
                        }
                      })
                    }
                  });
                })
                .catch(function (error) {
                  console.log(error);
                  return res.status(400).json({ error: { "code": 400, "message": "Duplicate box" } });
                });
            };
          }
        }
      );
    }
  } else {
    console.log({ "response": "some param is not defined" });
    return res.status(400).json({ error: { "code": 400, "message": "Register fail" } });
  }
});


// delete keptbox
app.post(__api_v + "keptboxs/delete", (req, res) => {
  console.log("--->/api/v1/keptboxs/delete");
  const sn_box = req.query.sn;
  console.log("[sn_box]", sn_box);
  con.query("DELETE FROM keptboxs WHERE box_sn = '" + sn_box + "'",
    function (err, data, fields) {
      if (err) {
        console.log("[error]", err);
        return res.status(400).json({ error: { "code": 400, "message": "Delete fail" } });
      } else {
        con.query("DELETE FROM devices WHERE box_sn = '" + sn_box + "'",
          function (err, data, fields) {
            if (err) {
              console.log("[error]", err);
              return res.status(400).json({ error: { "code": 400, "message": "Delete fail" } });
            } else {
              con.query("DELETE FROM mac_devices WHERE box_sn = '" + sn_box.toLowerCase() + "'",
                function (err, data, fields) {
                  if (err) {
                    console.log("[error]", err);
                    return res.status(400).json({ error: { "code": 400, "message": "Delete fail" } });
                  } else {
                    con.query("DELETE FROM opening_logs WHERE box_sn = '" + sn_box.toLowerCase() + "'",
                      function (err, data, fields) {
                        if (err) {
                          console.log("[error]", err);
                          return res.status(400).json({ error: { "code": 400, "message": "Delete fail" } });
                        } else {
                          console.log({ "message": "Delete Success" });
                          return res.status(200).json({ data: { "message": "Delete Success" } });
                        }
                      });
                  }
                });
            }
          });
      }
    });
});

//-----BOT------//
app.post(__api_v + "keptboxs/logs", (req, res) => {
  console.log("--->/api/v1/keptboxs/logs");
  const open_with = req.query.at; //หน้า menu bot
  const box_sn = req.query.sn;
  console.log("[keptboxs/logs] :" + open_with + ", " + box_sn);
  if (box_sn.length == 0) {
    console.log("[box sn] is undefined");
    res.send("box_sn is undefined");
  } else {
    var insert_logs = "INSERT INTO opening_logs (box_sn, open_with) VALUES ?";  /*.................*/
    var VALUES = [[box_sn.toLowerCase(), open_with]];
    // console.log(val_mac_devices);
    con.query(insert_logs, [VALUES], function (err, data, fields) {
      if (err) {
        console.log("[error]", err);
      } else {
        // console.log({ "message": "Update logs Success" });
        res.send("Update logs Success");
      }
    });
  }
});

//---- hook -- แก้ไขอุปกรณ์> qry อุปกรณ์ที่ผูกกับ one id นั้น ว่ามีกี่ device
app.post(__api_v + "hook", (req, res) => {
  console.log("--->/api/v1/hook");
  let hook = req.body;
  // console.log('Got body:', hook);
  if (hook.event === "message") {
    let message = hook.message.text;
    let message_data = hook.message.data;
    if (message === "ตั้งค่าเบอร์โทร") {
      let _message = "กรุณากรอกเบอร์มือถือ";
      Push_message(hook, _message);
    } else if (message === "เพิ่มอุปกรณ์") {
      let _message = 'เลือก "เพิ่มอุปกรณ์" เพื่อสร้างอุปกรณ์ของฉัน';
      let reply = [reply_webview("เพิ่มอุปกรณ์", "scanreg", "add_device"), reply_message("กลับสู่เมนูหลัก", "เมนูหลัก")];
      Quickreply(hook, _message, reply);
    } else if (message === "เมนูหลัก") {
      let _message = 'สามารถเลือกเมนูที่ต้องการได้เลยค่ะ';
      let reply = "";
      var sql = "SELECT box_sn, box_name FROM devices  WHERE created_by = '" + hook.source.email + "' AND user_permis = 'admin'";
      con.query(sql, function (err, data, fields) {
        if (err) {
          console.log("[error]", err);
        } else if (data.length === 0) {
          console.log(data.length);
          reply = [
            reply_message("เพิ่มอุปกรณ์", "เพิ่มอุปกรณ์"),
            reply_message("อุปกรณ์ของฉัน", "อุปกรณ์ของฉัน"),
          ];
        } else {
          console.log(data.length);
          reply = [
            reply_message("เพิ่มอุปกรณ์", "เพิ่มอุปกรณ์"),
            reply_message("แก้ไขอุปกรณ์", "แก้ไขอุปกรณ์"),
            reply_message("อุปกรณ์ของฉัน", "อุปกรณ์ของฉัน"),
            reply_message("ผู้ติดตามอุปกรณ์ของฉัน", "ผู้ติดตามอุปกรณ์")
          ];
        }
        Quickreply(hook, _message, reply);
      });
    } else if (message === "แก้ไขอุปกรณ์") {
      axios.get(BACKEND_URL + __api_v + "keptboxs?id=" + hook.source.email)
        .then(function (response) {
          console.log(response);
          let menu = "editbox";
          let _message = 'เลือกอุปกรณ์ที่ต้องการแก้ไข';
          let reply = [reply_message("กลับสู่เมนูหลัก", "เมนูหลัก")];
          // console.log({ "reply": reply });
          let box = response.data.data;
          console.log(box);
          let box_reply = [];
          for (let index = 0; index < box.length; index++) {
            const element = box[index];
            box_reply.push(reply_webview(element.box_name, element.box_sn, menu)); //
          }
          box_reply.push.apply(box_reply, reply); //add array
          Quickreply(hook, _message, box_reply); // sent quickreply
        })
        .catch(function (error) {
          console.log(" : 1026");
          console.log(error);
        });
    } else if (message === "อุปกรณ์ของฉัน") {  /* อุปกรณ์ของฉัน */
      console.log("อุปกรณ์ของฉัน");
      axios.get(BACKEND_URL + __api_v + "keptboxs?id=" + hook.source.email)
        .then(function (response) {
          // console.log(response);
          let box = response.data.data;
          console.log(box);
          if (box.length == 0) {
            Push_message(hook, "ไม่มีอุปกรณ์ คุณสามารถเพิ่มอุปกรณ์ใหม่ที่เมนูหลัก");
          } else {
            let element = [];
            for (let index = 0; index < box.length; index++) {
              const data = box[index];
              element.push(rep_element(data.box_name, data.location, data.box_sn, data.batt_level, data.user_permis)); //
            }
            // console.log(element);
            Template(hook, element);
          }
        })
        .catch(function (error) {
          console.log(" : 1048");
          console.log(error);
        });
    } else if (message === "สั่งเปิดกล่อง") {
      // console.log(data);
      // BACKEND_URL + __api_v + "keptlock/unlock?sn=" + sn_box
      axios.post(BACKEND_URL + __api_v + "keptlock/unlock?sn=" + message_data)
        .then(function (response) {
          console.log("[response]", response.data);
          setTimeout(() => {
            console.log("Delayed for 10 second.");
            var sql_check = "SELECT * FROM mac_devices  WHERE box_sn = '" + message_data.toLowerCase() + "'";
            con.query(sql_check, function (err, res, fields) {
              if (err) {
                console.log("[error]", err);
              } else {
                if (res.length !== 0) {
                  var string = JSON.stringify(res);
                  var _data = JSON.parse(string);
                  var getData = _data[0];
                  console.log({ "check status": res.length });
                  if (getData.status_lock == "lock success") {
                    Push_message(hook, "เปิดกล่องสำเร็จ");
                    axios.post(BACKEND_URL + __api_v + 'keptboxs/logs?at=onechat&sn=' + message_data.toLowerCase())  // logs unlock
                      .then(function (response) {
                        console.log({ "message": response.data });
                      })
                      .catch(function (error) {
                        console.log(error);
                      });
                  } else {
                    Push_message(hook, "กรุณาลองใหม่อีกครั้ง");
                  }
                } else {
                  console.log("[ no 'box_sn' in table 'mac_devices' ]");
                  Push_message(hook, "ไม่มีอุปกรณ์ กรุณาตรวจสอบอุปกรณ์ของท่าน");
                }
              }
            });
          }, "20000")
        })
        .catch(function (error) {
          console.log("[error]", error);
        });
      //check status set MAC
    } else if (message === "ประวัติการเปิดกล่อง") {
      var sql_check = "SELECT * FROM opening_logs WHERE box_sn = '" + message_data + "' ORDER BY id DESC LIMIT 10";
      con.query(sql_check, function (err, res, fields) {
        if (err) {
          console.log("[error]", err);
        } else {
          var string = JSON.stringify(res);
          var _data = JSON.parse(string);
          // console.log(_data);
          var message_logs = "";
          for (let i = _data.length - 1; i >= 0; i--) {
            // console.log(_data[i]);
            var logs = "\r\nเปิดกล่องผ่าน : " + _data[i].open_with + "\r\n" + toIsoString(new Date(_data[i].open_at)) + "\r\n";
            message_logs = message_logs + logs;
          }
          axios.get(BACKEND_URL + __api_v + "keptboxs?id=" + hook.source.email)
            .then(function (response) {
              let box = response.data.data;
              // console.log(box);
              for (let index = 0; index < box.length; index++) {
                // console.log(index + ":" + box[index].box_sn);
                if (box[index].box_sn == message_data) {
                  Push_message(hook, "ประวัติการเปิดกล่อง " + box[index].box_name);
                }
              }
            })
            .catch(function (error) {
              console.log(error);
            });
          setTimeout(() => {
            Push_message(hook, message_logs);
          }, 1000);
        }
      });
    } else if (message === "อนุญาต" && "data" in hook.message) {
      // console.log({ "message_data": message_data });
      var box_sn = message_data.substring(0, 15).toLowerCase();
      var user_id_user = message_data.substring(16, 49);
      var email_user = message_data.substring(50);
      // console.log({ "box_sn": box_sn });
      // console.log({ "user_id_user": user_id_user });
      // console.log({ "email_user": email_user });
      var sql_getBOX = "SELECT * FROM devices WHERE box_sn = '" + box_sn + "' AND user_permis = 'admin'";
      con.query(sql_getBOX, function (err, data, fields) {
        if (err) {
          console.log("[error]", err);
        } else {
          var string = JSON.stringify(data);
          var _data = JSON.parse(string);
          var getData = _data[0];
          // console.log({ "data": getData });
          var sql_insert_user = "INSERT INTO devices (box_sn, box_name, location, batt_level, created_by, user_permis, user_id) VALUES ?";
          var val_device = [[getData.box_sn, getData.box_name, getData.location, getData.batt_level, email_user, 'user', user_id_user]];
          con.query(sql_insert_user, [val_device], function (err, data, fields) {
            if (err) {
              console.log("[error]", err);
            } else {
              console.log("insert user success");
              let message_re = "คุณสามารถใช้งานกล่อง [ " + getData.box_name + " ] ได้แล้ว เลือก 'อุปกรณ์ของฉัน' เพื่อใช้งาน";
              let req_body = {
                "to": user_id_user,
                "bot_id": BOT_ID,
                "type": "text",
                "message": message_re,
                "custom_notification": "เปิดอ่านข้อความใหม่จากทางเรา"
              };
              axios.post('https://chat-api.one.th/message/api/v1/push_message', req_body, axiosConfig)
                .then(function (response) {
                  console.log({ "code": response.status, "message": response.statusText });
                })
                .catch(function (error) {
                  // console.log({ "error": error.response.data });
                });

            }
          });
        }
      });
    } else if (message === "เลิกติดตามอุปกรณ์" && "data" in hook.message) {
      // console.log(message_data);
      con.query("DELETE FROM devices WHERE box_sn = '" + message_data + "' AND user_id = '" + hook.source.user_id + "'",
        function (err, data, fields) {
          if (err) {
            console.log("[error]", err);
          } else {
            console.log("[delete]", "success");
            Push_message(hook, "เลิกติดตามกล่องสำเร็จ");
          }
        });
    } else if (message === "ผู้ติดตามอุปกรณ์" && "data" in hook.message) {
      // console.log(message_data);
      axios.get(BACKEND_URL + __api_v + "keptboxs?id=" + hook.source.email)
        .then(function (response) {
          let _message = 'เลือกอุปกรณ์ที่ต้องการ';
          let reply = [reply_message("กลับสู่เมนูหลัก", "เมนูหลัก")];
          // console.log({ "reply": reply });
          let box = response.data.data;
          let box_reply = [];
          for (let index = 0; index < box.length; index++) {
            const element = box[index];
            // box_reply.push(reply_webview(element.box_name, element.box_sn, menu)); //
            box_reply.push(reply_message(element.box_name, element.box_name, "subscriber_" + element.box_sn));
          }
          box_reply.push.apply(box_reply, reply); //add array
          Quickreply(hook, _message, box_reply); // sent quickreply
        })
        .catch(function (error) {
          console.log(error.response.data);
        });
    } else if (message === "ลบผู้ติดตาม" && "data" in hook.message) {
      var box_sn = message_data.substring(0, 15);
      var email = message_data.substring(16);
      // console.log(email);
      con.query("DELETE FROM devices WHERE box_sn = '" + box_sn + "' AND created_by = '" + email + "'",
        function (err, data, fields) {
          if (err) {
            console.log("[error]", err);
          } else {
            console.log("[delete]", "success");
            Push_message(hook, "ลบผู้ติดตาม " + email + " สำเร็จ");
          }
        });
    } else if ("data" in hook.message) {
      var ms_check = message_data.substring(0, 10);
      if (ms_check == "subscriber") {
        // console.log("subscribersubscribersubscribersubscriber");
        var box_sn = message_data.substring(11)
        // console.log("box_sn : " + box_sn);
        axios.get(BACKEND_URL + __api_v + "subscriber?sn=" + box_sn)
          .then(function (response) {
            let box = response.data.data;
            // console.log({ "users": box });
            if (box.length == 1) {
              Push_message(hook, "ไม่มีผู้ติดตามกล่อง : " + box[0].box_name);
            } else {
              let element = [];
              for (let index = 1; index < box.length; index++) {
                const data = box[index];
                element.push(rep_element_sub(data.created_by, data.created_at, data.box_sn)); //
              }
              // console.log(element);
              Template(hook, element);
            }

          })
          .catch(function (error) {
            console.log(error.response);
          });
      }
    } else {
      console.log('event:', hook);
    }
    res.send({ 'result': req.body });
  }
});

function toIsoString(date) {
  var tzo = -date.getTimezoneOffset(),
    dif = tzo >= 0 ? '+' : '-',
    pad = function (num) {
      var norm = Math.floor(Math.abs(num));
      return (norm < 10 ? '0' : '') + norm;
    };
  return 'วันที่ ' + date.getDate() +
    '/' + pad(date.getMonth() + 1) +
    '/' + pad(date.getFullYear()) +
    '  เวลา ' + pad(date.getHours()) +
    ':' + pad(date.getMinutes()) +
    ':' + pad(date.getSeconds())
};

function Push_message(hook, message) {
  // let botID = hook.bot_id;
  let user_id = hook.source.user_id;
  let req_body = {
    "to": user_id,
    "bot_id": BOT_ID,
    "type": "text",
    "message": message,
    "custom_notification": "เปิดอ่านข้อความใหม่จากทางเรา"
  };
  axios.post('https://chat-api.one.th/message/api/v1/push_message', req_body, axiosConfig)
    .then(function (response) {
      // console.log(response);
      console.log({ "code": response.status, "message": response.statusText });
    })
    .catch(function (error) {
      console.log({ "error": error });
    });
};

function Quickreply(hook, message, reply) {
  // console.log({ "hook": hook });
  // let botID = hook.bot_id;
  let oneID = hook.source.user_id;
  let req_body = {
    "to": oneID,
    "bot_id": BOT_ID,
    "message": message,
    "quick_reply": reply
  };
  axios.post('https://chat-api.one.th/message/api/v1/push_quickreply', req_body, axiosConfig)
    .then(function (response) {
      console.log({ "code": response.status, "message": response.statusText });
    })
    .catch(function (error) {
      console.log({ "error": error.message });
      console.log({ "error": error });
    });
};


function reply_webview(label, re_url, menu) {
  let url = "";
  // console.log(re_url);
  if (menu == "editbox") {
    url = FRONTEND_URL + "/edit?sn=" + re_url;  //front 
  } else if (menu == "openbox") {
    url = FRONTEND_URL + "/scan?sn=" + re_url;
  } else if (menu == "add_device") {
    url = FRONTEND_URL + "/scanreg";
  } else if (menu == "user_sub") {
    url = FRONTEND_URL + "/subscriber?sn=" + re_url;
  } else {
    url = "https://www.google.co.th/";
  }
  let reply = {
    "label": label,
    "type": "webview",
    "url": url,
    "size": "full",
    "sign": "false",
    "onechat_token": "true"
  }
  return reply;
};

// template
function rep_element(name, location, sn_box, batt, permis) {
  let ele;
  console.log({ "permissions : ": permis });
  if (permis == "admin") {
    ele = {
      "image": "",
      "title": name + " : 🔋 " + batt + "%", /* box name */
      "detail": "สถานที่ : " + location, /* box location */
      "choice":
        [{
          "label": "สั่งเปิดกล่อง",
          "type": "text",
          "payload": sn_box
        }, {
          "label": "สแกนเปิดกล่อง",
          "type": "webview",
          "url": FRONTEND_URL + "/Scan?sn=" + sn_box,
          "size": "full",
          "sign": "false",
          "onechat_token": "true"
        }, {
          "label": "แก้ไขอุปกรณ์",
          "type": "webview",
          "url": FRONTEND_URL + "/edit?sn=" + sn_box,
          "size": "full",
          "sign": "false",
          "onechat_token": "true"
        }, {
          "label": "ประวัติการเปิดกล่อง",
          "type": "text",
          "payload": sn_box
        }]
    }
  } else if (permis == "user") {
    ele = {
      "image": "",
      "title": name + " : 🔋 " + batt + "%", /* box name */
      "detail": "สถานที่ : " + location, /* box location */
      "choice":
        [{
          "label": "สั่งเปิดกล่อง",
          "type": "text",
          "payload": sn_box
        }, {
          "label": "สแกนเปิดกล่อง",
          "type": "webview",
          "url": "http://203.150.184.156:8002/Scan?sn=" + sn_box,
          "size": "full",
          "sign": "false",
          "onechat_token": "true"
        }, {
          "label": "เลิกติดตามอุปกรณ์",
          "type": "text",
          "payload": sn_box
        }, {
          "label": "ประวัติการเปิดกล่อง",
          "type": "text",
          "payload": sn_box
        }]
    }
  }
  return ele;
};

// template subscriber
function rep_element_sub(email_user, sub_at, sn_box) {
  var date = new Date(sub_at);
  var result = date.toLocaleDateString('th-TH', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });
  let ele = {
    "image": "",
    "title": email_user, /* box name */
    "detail": "วันที่ติดตาม : " + result, /* box location */
    "choice":
      [{
        "label": "ลบผู้ติดตาม",
        "type": "text",
        "payload": sn_box + "_" + email_user
      }]
  }
  return ele;
};

function reply_message(label, message, payload) {
  // console.log({ "pay": payload });
  let reply = "";
  if (payload === undefined) {
    // console.log("pay load undefined");
    reply = {
      "label": label,
      "type": "text",
      "message": message,
      "payload": message
    }
  } else {
    reply = {
      "label": label,
      "type": "text",
      "message": message,
      "payload": payload
    }
  }
  return reply;
};

function Template(hook, elements) {
  // let botID = hook.bot_id;
  let oneID = hook.source.one_id;
  let req_body = {
    "to": oneID,
    "bot_id": BOT_ID,
    "type": "template",
    "custom_notification": "เปิดอ่านข้อความใหม่จากทางเรา",
    "elements": elements
  };
  axios.post('https://chat-api.one.th/message/api/v1/push_message', req_body, axiosConfig)
    .then(function (response) {
      console.log({ "code": response.status, "message": response.statusText });
    })
    .catch(function (error) {
      console.log({ "error": error.response.data });
    });
};

function setMAC_Status(message) {
  // var mac_gateway = message.gateway;
  var mac_lock = message.sn;
  var box_sn = "lys" + mac_lock;
  // console.log({ "boxsn": box_sn });
  var _box_sn = box_sn.replace(/:/g, "");
  console.log({ "result": _box_sn });
  var sql_update_status_lock = "UPDATE mac_devices SET status ='match' WHERE mac_devices.box_sn = '" + _box_sn + "'";
  con.query(sql_update_status_lock, function (err, data, fields) {
    if (err) {
      console.log("[error]", err);
    } else {
      console.log({ "message": "match Success" });
    }
  });
};

// set string MAC
function str_MAC(mac) {
  var parts = mac.match(/.{1,2}/g);
  var new_value = parts.join(":");
  return new_value;
};

// app.get('/*', function (req,  res) {
//   res.sendFile(path + "/dist/" + "index.html");
//   console.log("req recv");
// });

// app.get('*', function (req, res) {
//   res.sendFile(path + "/dist/" + "index.html");
//   console.log("entry req recv");
// });


// start the server
// app.listen(port, () => console.log(`Listening on port ${port}`));

var httpServer = http.createServer(app);
// var httpsServer = https.createServer(credentials, app);

httpServer.listen(port, () => console.log(`Listening on port ${port}`));
// httpsServer.listen(s_port, () => console.log(`Listening on port ${s_port}`)); 